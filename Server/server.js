// Bring in all necessary objects from other scripts, so we can use their functions later
const Client = require("./client.js").Client;
const PacketBuilder = require("./packetBuilder.js").PacketBuilder;

// This is our server object, it contains everything our server needs to do and know
exports.Server = {
    // The port used to connect to the server, an array of all the clients, and the maximum number of users this server can hold
    port: 1111,
    clients: [],
    maxConnectedUsers: 8,

    // This starts the server, getting a reference to the game class and creating the socket needed to allow users to connect
    start(game) {
        this.game = game;

        this.socket = require("net").createServer({}, c => this.onClientConnect(c));
        this.socket.on("error", e => this.onError(e));
        this.socket.listen({port: this.port}, () => this.onStartListen());
    
    },
    // This function is run whenever a client connects to the server
    onClientConnect(socket) {
        console.log("New connection from " + socket.localAddress);
        // If the server is full, send the client a message saying so and disconnect them
        if(this.isServerFull()) {
            const packet = PacketBuilder.join(9);
            socket.end(packet);
        }
        // Otherwise, add them to the list and connect them to the socket
        else {
            

            const client = new Client(socket, this);
            this.clients.push(client);
        }
        
    },
    // This function is run whenever a client disconnects from the server
    onClientDisconnect(client) {

        // If the client was one of the players, set that player to null
        if(this.game.clientRed == client) this.game.clientRed = null;
        if(this.game.clientBlack == client) this.game.clientBlack = null;

        // Remove the client from the clients array
        const index = this.clients.indexOf(client);
        this.clients.splice(index, 1);
        console.log("Player disconnected");
    },

    // This function runs whenever there's an error, and logs the error to the console
    onError(e) {
        console.log("ERROR with listener: " + e);
    },
    // This function is run when the server begins listening for clients to connect
    onStartListen() {
        console.log("The server is now listening on port " + this.port);
    },
    // This function checks if the number of clients connected exceeds the maximum, and returns a boolean based on that check
    isServerFull() {
        return (this.clients.length >= this.maxConnectedUsers);
    },
    // This function takes in a client's desired username and runs it through a bunch of checks to make sure it's okay, and assigns the client a role if it is
    generateResponseID(desiredUsername, client) {
        // Check for bad usernames
        if(desiredUsername.length < 3) return 4; // Too short
        if(desiredUsername.length > 18) return 5; // Too long
        
        const regex1 = /^[a-zA-Z0-9\[\]]+$/;
        if(!regex1.test(desiredUsername)) return 6; // Invalid characters

        let isUsernameTaken = false;
        this.clients.forEach(c=> {
            if(c == client) return;
            if(c.username ==desiredUsername) isUsernameTaken = true;
        });
        if(isUsernameTaken) return 7; // Already taken
        
        const regex2 = /(fuck|shit|damn|hell|piss|cunt)/i;

        if(regex2.test(desiredUsername)) return 8; // Swear filter

        // Set users as roles
        if(this.game.clientRed == client) return 1; // Red player reconnecting
        if(this.game.clientBlack == client) return 2; // Black player reconnecting

        if(this.game.clientRed && this.game.clientBlack) return 3; // Spectator

        // If there's no red player, assign them to the incoming client
        if(!this.game.clientRed){
            this.game.clientRed = client;
            return 1;
        }
        // If there's no black player, assign them to the incoming client
        if(!this.game.clientBlack) {
            this.game.clientBlack = client;
            return 2;
        }

        // Default to spectator
        return 3;

    },
    // This function sends a packet to all connected clients
    broadcastPacket(packet) {
        
        this.clients.forEach(c => {
            c.sendPacket(packet);
        });
    }
    

    

};