// This object builds packets for the server to send to the client (see protocol for more details)
exports.PacketBuilder = {
    join(responseID) {
        const packet = Buffer.alloc(5);

        packet.write("JOIN", 0);
        packet.writeUInt8(responseID, 4);

        return packet;
    },
    chat(user, message) {
        const packet = Buffer.alloc(7 + user.length + message.length);
        packet.write("CHAT", 0);
        packet.writeUInt8(user.length, 4);
        packet.writeUInt16BE(message.length, 5);
        packet.write(user, 7);
        packet.write(message, 7 + user.length);
        return packet;
    },
    update(game) {
        const packet = Buffer.alloc(48);
        packet.write("UPDT", 0);
        packet.writeUInt8(game.currTurn, 4);
        packet.writeUInt8(game.winner, 5);
        let offset = 6;
        for(let y = 0; y < game.board.length; y++) {
            for(let x = 0; x < game.board[y].length; x++) {
                packet.writeUInt8(game.board[y][x], offset);
                // This console log prevents the game from being several steps behind. I don't understand why, but it's important
                console.log(game.board[y][x]);
                offset++;
            }
        }
        return packet;
    }
}