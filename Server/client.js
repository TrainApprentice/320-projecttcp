// Bring in all necessary objects from other scripts, so we can use their functions later

const PacketBuilder = require("./packetBuilder.js").PacketBuilder;

// This is our client class, it holds the client information on the server side for server reference and communicates with the Unity client
exports.Client = class Client {
    // Create the client class with reference to the server and its socket
    constructor(socket, server) {
        this.socket = socket;
        this.server = server;

        this.username = "";
        this.buffer = Buffer.alloc(0);

        this.socket.on("error", (e) => this.onError(e));
        this.socket.on("close", () => this.onClose());
        this.socket.on("data", (d) => this.onData(d));

        
    }
    // This function is run if there's an error with the client
    onError(msg) {
        console.log("ERROR with client: " + msg);
    };
    // This function is run if the connection to the server is closed
    onClose() {
        this.server.onClientDisconnect(this);
    };
    // This function is run whenever data is taken in by the client, and does different things based on the data
    onData(data) {
        // Add this data to the buffer
        this.buffer = Buffer.concat([this.buffer, data]);

        // If the buffer is too small, we can't read it, so jump out of the function
        if(this.buffer.length < 4) return;

        // Get the packet header to figure out what this data is supposed to be
        const packetIndentifier = this.buffer.slice(0,4).toString();
        
        // Based on the packet header, do stuff
        const packetIdentifier = this.buffer.slice(0,4).toString();
        
        //console.log(packetIndentifier);
        switch(packetIdentifier) {
            case "JOIN":
                // If the packet is too small to be a valid JOIN packet, stop the process
                if(this.buffer.length < 5) return;

                const lengthOfUsername = this.buffer.readUInt8(4);

                if(this.buffer.length < 5 + lengthOfUsername) return;

                // Grab the desired username from the buffer
                const usernameInput = this.buffer.slice(5, 5 + lengthOfUsername).toString();
                
                console.log("Client wants to be named " + usernameInput);

                // Based on the desire username, generate a response (see server.js for more details)
                let responseType = this.server.generateResponseID(usernameInput, this);

                // Clear the buffer
                this.buffer = this.buffer.slice(5 + lengthOfUsername);
                 
                // Build a response packet for the username check and send it
                const packet = PacketBuilder.join(responseType);

                this.sendPacket(packet);

                // If the username was accepted, build another response packet to update the game
                if(responseType <= 3){
                    this.username = usernameInput;
                    const packet2 = PacketBuilder.update(this.server.game);
                    this.sendPacket(packet2);
                }
                

                break;
            case "CHAT":
                // If the packet is too small to be a valid CHAT packet, stop the process
                if(this.buffer.length < 5) return;
                const lengthOfMessage = this.buffer.readUInt8(4);
                if(lengthOfMessage <= 0) return;
                if(this.buffer.length < 5 + lengthOfMessage) return;

                // Grab the message from the buffer
                const message = this.buffer.slice(5, 5 + lengthOfMessage).toString();

                // Clear the buffer
                this.buffer = this.buffer.slice(5 + lengthOfMessage);
                
                // Build a chat packet and broadcast it to all other clients
                const packet3 = PacketBuilder.chat(this.username, message);
                this.server.broadcastPacket(packet3);
                break;
            case "PLAY":
                // If the packet is too small to be a valid PLAY packet, stop the process
                if(this.buffer.length < 6) return;

                // Grab the x and y positions of the updated board piece from the buffer
                const x = this.buffer.readUInt8(4);
                const y = this.buffer.readUInt8(5);

                // Clear the buffer
                this.buffer = this.buffer.slice(6);

                // Get the server to update the game
                this.server.game.playMove(this, x, y);

                break;
            default:
                console.log("ERROR: packet identifier not recognized: (" + packetIndentifier + ")");
                this.buffer = Buffer.alloc(0);
                break;
        }
    };
    // This function sends a packet to the server
    sendPacket(packet) {
        this.socket.write(packet);
    }

}