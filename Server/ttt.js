// Bring in all necessary objects from other scripts, so we can use their functions later

const server = require("./server").Server;
const PacketBuilder = require("./packetBuilder.js").PacketBuilder;

// This is the game object, where all information about the game board is stored server-side
const Game = {
    // Keeps track of the turn, winner, board values, and each player
    currTurn: 1,
    winner: 0,
    board: [
        [0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0]
    ],
    clientRed: null,
    clientBlack: null,

    // This takes in the client's input and updates the board
    playMove(client, x, y) {
        // Check to make sure it's a valid play
        if(this.winner > 0) return;

        if(this.currTurn == 1 && client != this.clientRed) return;
        if(this.currTurn == 2 && client != this.clientBlack) return;

        if(x < 0) return;
        if(y < 0) return;

        if(y >= this.board.length) return;
        if(x >= this.board[y].length) return;

        if(this.board[y][x] > 0) return;

        // Update the board piece with who clicked it
        this.board[y][x] = this.currTurn;
        
        // Check every board piece to see if the board meets a win condition
        for(let i = 0; i < this.board.length; i++) {
            for(let j = 0; j < this.board[i].length; j++) {
                this.checkVictor(j, i);
            }
        }
        
        // Swap the current turn
        this.currTurn = (this.currTurn == 1) ? 2 : 1;
        
        // Send an UPDT packet to the server
        this.checkStateAndUpdate();
    },
    // This funciton
    checkStateAndUpdate() {
        
        // Build the update packet and send it
        const packet = PacketBuilder.update(this);
        server.broadcastPacket(packet);

        // If there's a winner, reset the board alongside the UPDT packet
        if(this.winner != 0) {
            for(let i = 0; i < this.board.length; i++) {
                for(let j = 0; j < this.board[i].length; j++) {
                    this.board[i][j] = 0;
                }
            }
            this.winner = 0;
        }
    },
    // This function checks a position on the board for all possible win scenarios
    checkVictor(x, y) {
        if(this.currTurn == this.board[y][x]) {
            // Vertical check (upwards)
            if (y <= 2 && this.currTurn == this.board[y + 1][x]  && this.currTurn == this.board[y + 2][x]  && this.currTurn == this.board[y + 3][x] ) {
               this.winner = this.currTurn;
            }
            // Vertical check (downwards) 
            else if (y >= 3 && this.currTurn == this.board[y - 1][x]  && this.currTurn == this.board[y - 2][x]  && this.currTurn == this.board[y - 3][x] ) {
               this.winner = this.currTurn;
            }
            // Horizontal check 
            else if (x >= 3 && this.currTurn == this.board[y][x - 1] && this.currTurn == this.board[y][x - 2] && this.currTurn == this.board[y][x - 3] ) {
               this.winner = this.currTurn;
            } 
            // Diagonal check (left & up)
            else if (y <= 2 && x >= 3 && this.currTurn == this.board[y + 1][x - 1]  && this.currTurn == this.board[y + 2][x - 2]  && this.currTurn == this.board[y + 3][x - 3] ) { 
               this.winner = this.currTurn;
            }
            // Diagonal check (right & up) 
            else if (y <= 2 && x <= 3 && this.currTurn == this.board[y + 1][x + 1]  && this.currTurn == this.board[y + 2][x + 2]  && this.currTurn == this.board[y + 3][x + 3] ) {
               this.winner = this.currTurn;
            }
            // Diagonal check (left & down)
            else if (y >= 3 && x >= 3 && this.currTurn == this.board[y - 1][x - 1]  && this.currTurn == this.board[y - 2][x - 2]  && this.currTurn == this.board[y - 3][x - 3] ) { 
               this.winner = this.currTurn;
            }
            // Diagonal check (right & up) 
            else if (y >= 3 && x <= 3 && this.currTurn == this.board[y - 1][x + 1]  && this.currTurn == this.board[y - 2][x + 2]  && this.currTurn == this.board[y - 3][x + 3] ) {
               this.winner = this.currTurn;
            }
            // If there's no win, update the counter
            else {
                // If the number of filled spaces matches the total, and there's no winner, it's a draw
                let count = 0;
                for(let i = 0; i < this.board.length; i++) {
                    for(let j = 0; j < this.board[i].length; j++) {
                        if(this.board[i][j] != 0) count++;
                    }
                }
                if(count == 42) this.winner = 3;
            }
        }
        
    }

}

// Begin the server
server.start(Game);

