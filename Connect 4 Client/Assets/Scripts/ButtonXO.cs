using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

// This script holds information for an individual button
public struct GridPos
{
    public int x, y;

    public GridPos(int X, int Y)
    {
        this.x = X;
        this.y = Y;
    }
}

// This is the actual button class
public class ButtonXO : MonoBehaviour
{
    public GridPos pos;
    public Button button;

    public Image img;
    public int owner = 0;

    // This function is run when a button is created
    public void Init(GridPos pos, UnityAction callback)
    {
        button.onClick.AddListener(callback);
        this.pos = pos;
    }

    // This function updates the information stored in the button based on which player clicked it
    public void SetOwner(byte b)
    {
        if (b == 0) img.color = Color.white;
        if (b == 1) img.color = Color.red;
        if (b == 2) img.color = Color.black;
        owner = b;
    }


}
