using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public ButtonXO tile;
    private ButtonXO[,] boardUI;

    public Transform panelGameBoard;
    public TextMeshProUGUI whoseTurn;

    // Start is called before the first frame update
    void Start()
    {
        BuildBoardUI(7, 6);
    }

    // This function builds the board for the gameplay screen
    private void BuildBoardUI(int col, int row)
    {
        boardUI = new ButtonXO[col, row];

        for(int i = 0; i < row; i++)
        {
            for(int j = 0; j < col; j++)
            {
                ButtonXO newButton = Instantiate(tile, panelGameBoard);

                newButton.Init(new GridPos(j, i), () => { ButtonClicked(newButton); });
                boardUI[j, i] = newButton;

            }
        }
    }

    // This function is added to each board tile and sends a PLAY packet to the server based on the button clicked
    private void ButtonClicked(ButtonXO newButton)
    {
        int xPos = newButton.pos.x;
        int yPos = 5;

        // This determines the lowest available spot in the selected column to set a piece in
        for(int i = yPos; i >= 0; i--)
        {
            if(boardUI[xPos, i].owner == 0)
            {
                yPos = i;
                break;
            }
        }
        // Tell the GameClient script to send a PLAY packet to the server
        GameClient.singleton.SendPlayPacket(xPos, yPos);
    }

    // This function takes and reads UPDT packets from the server
    public void UpdateFromServer(byte gameState, byte turn, byte[] spaces)
    {
        // Update the board pieces to make sure they know who owns them
        for (int i = 0; i < spaces.Length; i++)
        {
            byte b = spaces[i];

            int x = i % 7;
            int y = Mathf.FloorToInt(i / 7);

            boardUI[x, y].SetOwner(b);
        }
        // Update a piece of text to tell the players whose turn it is
        whoseTurn.text = (turn == 1) ? "Red's Turn" : "Black's Turn";

        // If a winner has been determined, display the win screen
        if(gameState != 0)
        {
            GameClient.singleton.DisplayWinner(gameState);
        }
    }
  

   
}
