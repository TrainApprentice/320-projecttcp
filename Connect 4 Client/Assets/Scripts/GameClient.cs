using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using TMPro;
using System;

public class GameClient : MonoBehaviour
{

    public static GameClient singleton;

    public TMP_InputField inputHost, inputPort, inputUsername, sendMessage;
    public GameObject connectionPanel, usernamePanel, gamePanel, winnerPanel;

    public TextMeshProUGUI chatDisplay, winDisplay, errorMessage, gameFullMsg;

    public GameManager game;

    private Buffer buffer = Buffer.Alloc(0);

    private TcpClient socket;

    public enum Panel
    {
        Connection, 
        Username,
        Gameplay
    }
    // This function runs when the game initializes, and sets up the singleton for this class
    void Awake()
    {
        if(singleton)
        {
            Destroy(gameObject);
        }

        else
        {
            singleton = this;
            DontDestroyOnLoad(gameObject);
        }
        GoToPanel(Panel.Connection);
    }

    // This function runs when the Connect button is clicked on the Connection Panel
    public void OnButtonConnect()
    {
        // Create a new socket
        socket = new TcpClient();

        // Store the host address
        string host = inputHost.text;

        // Make sure the port is a valid number
        UInt16.TryParse(inputPort.text, out ushort port);

        // Try to connect to the server at the given host and port
        TryConnect(host, port);
    }

    // This function runs when the Submit butotn is clicked on the Username Panel
    public void OnUsernameButton()
    {
        // Store the user's name
        string user = inputUsername.text;

        // Build a JOIN packet to send to the server and send it
        Buffer packet = PacketBuilder.Join(user);
        SendPacketToServer(packet);
    }

    // This function runs whenever a chat is submitted to be sent to all clients
    public void OnChatInput()
    {
        // Store the chat
        string input = sendMessage.text;

        // Build a CHAT packet to send to the server and send it
        Buffer packet = PacketBuilder.Chat(input);
        SendPacketToServer(packet);

        // Clear the chat from the input field
        sendMessage.text = "";
    }

    // This function is run whenever the user clicks the Leave button in the Winner Panel on the Gameplay Panel
    public void OnButtonLeave()
    {
        socket.Close();
        print(socket.Connected);
        winnerPanel.SetActive(false);
        chatDisplay.text = "";
        GoToPanel(Panel.Connection);
        
    }

    // This function is run when the client tries to connect to the server
    public async void TryConnect(string host, int port)
    {
        // If we've already connected, stop the process
        if (socket.Connected) return;
        try
        {
            // Wait for the attempt to connect
            await socket.ConnectAsync(host, port);

            // If it works, send them to the username panel and start receiving packets from the server
            GoToPanel(Panel.Username);
            StartReceivingPackets();
        }
        catch (Exception e)
        {
            // If anything goes wrong, say so and send them back to the connection panel
            print("FAILED TO CONNECT");
            GoToPanel(Panel.Connection);
        }
        
        
    }

    // This function runs continuously once the client has connected to the server
    private async void StartReceivingPackets()
    {
        int maxPacketSize = 4096;

        // While we're connected, read packets
        while(socket.Connected)
        {
            byte[] data = new byte[maxPacketSize];
            try
            {
                // If we have a new packet to read, read it and add it to the buffer
                int bytesRead = await socket.GetStream().ReadAsync(data, 0, maxPacketSize);

                buffer.Concat(data, bytesRead);

                // Process the packet we've received
                ProcessPackets();

            }
            // If we can't read the data, something went wrong
            catch (Exception e)
            {
                print("ERROR HANDLING DATA: " + e);
            }

        }
    }

    // This function sends a packet back to the server
    public async void SendPacketToServer(Buffer packet)
    {
        if (!socket.Connected) return;

        await socket.GetStream().WriteAsync(packet.Bytes, 0, packet.Bytes.Length);
    }

    // This function is run whenever a new packet is sent from the server, and updates the game after deconstructing the packet
    private void ProcessPackets()
    {
        // Make sure the packet is a valid length
        if (buffer.Length < 4) return;

        // Store the packet header
        string identifier = buffer.ReadString(0, 4);

        switch(identifier)
        {
            case "JOIN":
                // Make sure it's a valid length for a JOIN packet
                if (buffer.Length < 5) return;
                byte joinResponse = buffer.ReadByte(4);

                // If the join response is good, send the client to the game
                if(joinResponse <= 3)
                {
                    SetError(joinResponse);
                    gameFullMsg.text = "";
                    GoToPanel(Panel.Gameplay);
                }
                // If the game is full, send the client back to the connection panel
                else if(joinResponse == 9)
                {
                    GoToPanel(Panel.Connection);
                    gameFullMsg.text = "Game is full, try again later";
                }
                // If the join response is bad, tell the client what went wrong
                else
                {
                    SetError(joinResponse);
                    gameFullMsg.text = "";
                    GoToPanel(Panel.Username);
                }

                // Remove that packet from the buffer
                buffer.Consume(5);

                break;
            case "CHAT":
                // Make sure it's a valid length for a CHAT packet
                if (buffer.Length < 7) return;

                byte usernameLength = buffer.ReadByte(4);

                ushort messageLength = buffer.ReadUInt16BE(5);
           
                int fullLength = 7 + usernameLength + messageLength;

                if (buffer.Length < fullLength) return;

                // Add the message to the chat
                string username = buffer.ReadString(7, usernameLength);
                string message = buffer.ReadString(7 + usernameLength, messageLength);

                SendMessageInChat(username, message);

                // Make sure we're still in the game
                GoToPanel(Panel.Gameplay);

                // Remove that packet from the buffer
                buffer.Consume(fullLength);


                break;
            case "UPDT":
                // Make sure it's a valid length for a UPDT packet
                if (buffer.Length < 48) return;

                // Store all the information about gamestates from the packet
                byte turn = buffer.ReadByte(4);
                byte gameState = buffer.ReadByte(5);

                byte[] spaces = new byte[42];
                for (int i = 0; i < 42; i++)
                {
                    spaces[i] = buffer.ReadUInt8(6 + i);
                }

                // Make sure we're still in the game
                GoToPanel(Panel.Gameplay);

                // Update the actual game information
                game.UpdateFromServer(gameState, turn, spaces);
                // Remove that packet from the buffer
                buffer.Consume(48);

                break;
            default:
                print("Unknown identifier");
                buffer.Clear();
                break;
        }
    }

    // This function is purely client side and sets the current screen to the input
    public void GoToPanel(Panel p)
    {
        if(p == Panel.Connection)
        {
            gamePanel.SetActive(false);
            connectionPanel.SetActive(true);
            usernamePanel.SetActive(false);
            winnerPanel.SetActive(false);
        }
        if (p == Panel.Gameplay)
        {
            gamePanel.SetActive(true);
            connectionPanel.SetActive(false);
            usernamePanel.SetActive(false);
        }
        if (p == Panel.Username)
        {
            gamePanel.SetActive(false);
            connectionPanel.SetActive(false);
            usernamePanel.SetActive(true);
        }

    }

    // This function specifically builds and sends a PLAY packet to update the game server-side
    public void SendPlayPacket(int x, int y)
    {
        SendPacketToServer(PacketBuilder.Play(x, y));
    }

    // This function adds a given message from a user to the chat display
    public void SendMessageInChat(string user, string message)
    {
        chatDisplay.text += user + ": " + message + "\n";
    }

    // This function unhides the Winner panel whent the game is won, and updates the text according to the winner
    public void DisplayWinner(int winner)
    {
        winnerPanel.SetActive(true);
        switch(winner)
        {
            case 1:
                winDisplay.text = "Red wins!";
                break;
            case 2:
                winDisplay.text = "Black wins!";
                break;
            case 3:
                winDisplay.text = "Tie game!";
                break;
            default:
                winDisplay.text = "Disconnected...";
                break;
        }


    }

    // This function updates some text to let the player know why their username was rejected
    public void SetError(int response)
    {
        switch(response)
        {
            case 4:
                errorMessage.text = "Username too short";
                break;
            case 5:
                errorMessage.text = "Username too long";
                break;
            case 6:
                errorMessage.text = "Invalid characters";
                break;
            case 7:
                errorMessage.text = "Username already taken";
                break;
            case 8:
                errorMessage.text = "No profanity, please";
                break;
            default:
                errorMessage.text = "";
                break;
        }
    }
}
